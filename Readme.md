# Programowanie gier projekt zaliczeniowy

## Autor
imię i nazwisko: Jakub Młynarz <br />
numer indeksu: 127220 <br />
mail: jakub.mlynarz@student.put.poznan.pl

## O grze
Gra jest platformówką inspirowaną grą Mario. Żeby wygrać należy przejść do końca mapy. Świat, czyli układ platform i przeciwników jest generowany losowo za każdym uruchomieniem nowej gry. Można zabijać przeciwników skacząc na nich od gór, za to otrzymuje się punkty, każdy inny kontakt z przeciwnikiem kończy się stratą życia i przeniesieniem na początek mapy. W przypadku śmierci nie jest resetowany wynik o ile gracz ma jeszcze jakieś życia, w przeciwnym razie następuje koniec gry. 

## Sterowanie
- strzałka w lewo - ruch w lewo
- strzała w prawo - ruch w prawo
- strzłka do góry lub spacja - skok

## Uruchamianie projektu
Żeby uruchomić projekt potrzebny jest node i npm.

1. Instalowanie zależności
```
npm i
```
2. Uruchamianie projektu
```
npm start
```