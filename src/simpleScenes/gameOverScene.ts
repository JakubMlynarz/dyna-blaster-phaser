import * as Phaser from 'phaser';
import { SCREEN_WIDTH, SCREEN_HEIGHT} from "../const";

const sceneConfig: Phaser.Types.Scenes.SettingsConfig = {
    active: false,
    visible: false,
    key: 'GameOver',
  };


  export class GameOverScene extends Phaser.Scene {
    private playButton: Phaser.GameObjects.Text;
    private score: number = 0;

    constructor() {
      super(sceneConfig);
    }

    init(data){
      this.score = data.score;
    }

    public preload() {
      this.load.image('titlebg', 'assets/titlebg.png');
    }

    public create() {
      this.add.tileSprite(SCREEN_WIDTH / 2 , SCREEN_HEIGHT / 2, SCREEN_WIDTH, SCREEN_HEIGHT, 'titlebg');
      this.add.text((SCREEN_WIDTH / 2) - 125 , 100, 'Game over!', {fontSize: 40, color: "red"});
      this.add.text((SCREEN_WIDTH / 2) - 110 , 150, 'Your score: ' + this.score, {fontSize: 25, color: "red"});
      this.playButton = this.add.text((SCREEN_WIDTH / 2) - 120 , 400, 'Play again', {fontSize: 30, color: "blue", backgroundColor: "green", padding: 10, borderRadius: 10});
      this.playButton.setInteractive({ useHandCursor: true });

      this.playButton.on('pointerdown', () => {
        this.startGame();
      })
    }

    public update() {

    }

    private startGame() {
      this.scene.start('Game');
    }
  }