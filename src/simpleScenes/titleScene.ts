import * as Phaser from 'phaser';
import { SCREEN_WIDTH, SCREEN_HEIGHT} from "../const";

const sceneConfig: Phaser.Types.Scenes.SettingsConfig = {
    active: false,
    visible: false,
    key: 'Title',
  };


  export class TitleScene extends Phaser.Scene {
    private playButton: Phaser.GameObjects.Text;

    constructor() {
      super(sceneConfig);
    }

    public preload() {
      this.load.image('titlebg', 'assets/titlebg.png');
    }

    public create() {
      this.add.tileSprite(SCREEN_WIDTH / 2 , SCREEN_HEIGHT / 2, SCREEN_WIDTH, SCREEN_HEIGHT, 'titlebg');
      this.add.text((SCREEN_WIDTH / 2) - 205 , 125, 'Shapes and colors!', {fontSize: 40, color: "red"});
      this.playButton = this.add.text((SCREEN_WIDTH / 2) - 30 , 200, 'Play', {fontSize: 30, color: "blue", backgroundColor: "green", padding: 10, borderRadius: 10});
      this.playButton.setInteractive({ useHandCursor: true });

      this.playButton.on('pointerdown', () => {
        this.startGame();
      })
    }

    public update() {

    }

    private startGame() {
      this.scene.start('Game');
    }
  }