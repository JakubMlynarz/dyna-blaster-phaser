import * as Phaser from 'phaser';
import { GameScene }  from "./game/gameScene";
import { TitleScene }  from "./simpleScenes/titleScene";
import { GameOverScene }  from "./simpleScenes/gameOverScene";
import { GameEndScene }  from "./simpleScenes/gameEndScene";
import { SCREEN_WIDTH, SCREEN_HEIGHT } from "./const";

const gameConfig: Phaser.Types.Core.GameConfig = {
  title: 'Sample',
 
  type: Phaser.AUTO,
  scene:  [TitleScene, GameScene, GameOverScene, GameEndScene],
  scale: {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
  },
 
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 500 },
      debug: true,
    },
  },
 
  parent: 'game',
  backgroundColor: '#000000',
};
 
export const game = new Phaser.Game(gameConfig);