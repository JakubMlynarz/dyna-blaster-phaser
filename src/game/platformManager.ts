import * as Phaser from 'phaser';
import { GameScene }  from "./gameScene";
import { SCREEN_HEIGHT, GAME_LENGTH} from "../const";
import { ALL_PLATFORMS } from "./platforms";
import { getRandomInt } from "../utils";

export class PlatformManager {
    platforms: Phaser.Physics.Arcade.Sprite[] = [];
    gameScene: GameScene;

    constructor(gameScene: GameScene) {
        this.gameScene = gameScene; 
    }


    public addPlatforms() {
        let lastPlatformX = 0;
        while(lastPlatformX < GAME_LENGTH - 1300) {
            let variant  = getRandomInt(1, 6);
            let offset = getRandomInt(450, 550);
            switch(variant) {
                case 1:
                    this.generateVariant1(lastPlatformX + offset);
                    break;
                case 2:
                    this.generateVariant2(lastPlatformX + offset);
                    break;
                case 3:
                    this.generateVariant3(lastPlatformX + offset);
                    break;
                case 4:
                    this.generateVariant4(lastPlatformX + offset);
                    break;
                case 5:
                    this.generateVariant5(lastPlatformX + offset);
                    break;
                default:
                    this.generateVariant1(lastPlatformX + offset);
            }
            lastPlatformX = this.getPrevPlatform().x;
        }
    }

    private addPlatform(x: number, y: number, texture: string=this.getRandomPlatformName()) {
        let platform: Phaser.Physics.Arcade.Sprite = this.gameScene.physics.add.sprite(x,y,texture);
        platform.setDepth(1);
        platform.setImmovable(true);
        (platform.body as Phaser.Physics.Arcade.Body).allowGravity = false
        this.gameScene.physics.add.existing(platform);
        this.gameScene.boundsGroup.add(platform);
        this.platforms.push(platform);
    }

    private generateVariant1(x: number) {
        this.gameScene.enemyManager.addEnemOnGround(x);
        this.addPlatform(
            x + getRandomInt(120, 150), 
            SCREEN_HEIGHT - getRandomInt(180, 190), 
            this.getRandomPlatformName());
        if(this.randomBoolean()) {
            this.gameScene.enemyManager.addEnemy(this.platforms.length - 1);
        }
        if(this.randomBoolean()) {
            this.gameScene.enemyManager.addEnemOnGround(x + 500);
        }
    }

    private generateVariant2(x: number) {
        this.gameScene.enemyManager.addEnemOnGround(x);
        this.addPlatform(
            x + getRandomInt(120, 150), 
            SCREEN_HEIGHT - getRandomInt(180, 190), 
            this.getRandomPlatformName());
        this.gameScene.enemyManager.addEnemy(this.platforms.length - 1);
        this.addPlatform(
            this.getPrevPlatform().x + getRandomInt(250, 400), 
            this.getPrevPlatform().y - getRandomInt(120, 150), 
            this.getRandomPlatformName());
        if(this.randomBoolean()) {
            this.gameScene.enemyManager.addEnemOnGround(x + 500);
        }
        if(this.randomBoolean()) {
            this.gameScene.enemyManager.addEnemy(this.platforms.length - 1);
        }
    }

    private generateVariant3(x: number) {
        this.gameScene.enemyManager.addEnemOnGround(x);
        this.addPlatform(
            x + getRandomInt(120, 150), 
            SCREEN_HEIGHT - getRandomInt(180, 190), 
            this.getRandomPlatformName());
        this.addPlatform(
            this.getPrevPlatform().x + getRandomInt(250, 400), 
            this.getPrevPlatform().y - getRandomInt(120, 150), 
            this.getRandomPlatformName());
        this.gameScene.enemyManager.addEnemy(this.platforms.length - 1);
        this.addPlatform(
            this.getPrevPlatform().x + getRandomInt(250, 400) , 
            this.getPrevPlatform().y - getRandomInt(120, 150), 
            this.getRandomPlatformName());
        if(this.randomBoolean()) {
            this.gameScene.enemyManager.addEnemy(this.platforms.length - 1);
        }
        if(this.randomBoolean()) {
            this.gameScene.enemyManager.addEnemOnGround(x + 500);
        }
        if(getRandomInt(0, 4)  === 1) {
            this.addPlatform(
                this.platforms[this.platforms.length - 2].x + getRandomInt(50, 70),  
                SCREEN_HEIGHT - 150, 
                'column');
        }
    }

    private generateVariant4(x: number) {
        this.gameScene.enemyManager.addEnemOnGround(x);
        this.addPlatform(
            x + getRandomInt(120, 150), 
            SCREEN_HEIGHT - getRandomInt(180, 190), 
            this.getRandomPlatformName());
        this.gameScene.enemyManager.addEnemy(this.platforms.length - 1);
        this.addPlatform(
            this.getPrevPlatform().x + getRandomInt(250, 400), 
            this.getPrevPlatform().y - getRandomInt(120, 150), 
            this.getRandomPlatformName());
        this.addPlatform(
            this.getPrevPlatform().x + getRandomInt(250, 400),  
            SCREEN_HEIGHT - getRandomInt(180, 190), 
            this.getRandomPlatformName());
        if(this.randomBoolean()) {
            this.gameScene.enemyManager.addEnemy(this.platforms.length - 1);
        }
        if(getRandomInt(0, 3)  === 1) {
            this.addPlatform(
                this.platforms[this.platforms.length - 2].x + getRandomInt(50, 70),  
                SCREEN_HEIGHT - 150, 
                'column');
        }
    }

    private generateVariant5(x: number) {
        this.gameScene.enemyManager.addEnemOnGround(x);
        this.addPlatform(
            x + getRandomInt(120, 150), 
            SCREEN_HEIGHT - getRandomInt(180, 190), 
            this.getRandomPlatformName());
        if(this.randomBoolean()) {
            this.gameScene.enemyManager.addEnemy(this.platforms.length - 1);
        }
        this.addPlatform(
            this.getPrevPlatform().x + getRandomInt(250, 400), 
            this.getPrevPlatform().y - getRandomInt(120, 150), 
            this.getRandomPlatformName());
        this.gameScene.enemyManager.addEnemy(this.platforms.length - 1);
        this.addPlatform(
            this.getPrevPlatform().x + getRandomInt(250, 400) , 
            this.getPrevPlatform().x - getRandomInt(120, 150), 
            this.getRandomPlatformName());

        if(this.randomBoolean()) {
            this.gameScene.enemyManager.addEnemy(this.platforms.length - 1);
        }
        if(this.randomBoolean()) {
            this.gameScene.enemyManager.addEnemOnGround(x + 500);
        }
        this.addPlatform(
            this.platforms[this.platforms.length - 2].x + getRandomInt(50, 70),  
            SCREEN_HEIGHT - 150, 
            'column');

    }

    private randomBoolean() : boolean {
        return getRandomInt(0,2) === 0;
    }


    private getPrevPlatform(): Phaser.Physics.Arcade.Sprite {
        return this.platforms[this.platforms.length - 1];
    }

    private getRandomPlatformName(): string {
        return ALL_PLATFORMS[getRandomInt(0, ALL_PLATFORMS.length)];
    }
}