export enum Platforms {
    RED = "redplatform",
    BLUE = "blueplatform",
    PURPLE = "purpleplatform",
    YELLOW = "yellowplatform",
    GREEN = "greenplatform"
}

export function getPlatformAssetPath(platform: string): string {
    return 'assets/platforms/' + platform + '.png';
}

export const ALL_PLATFORMS: string[] = [Platforms.RED, Platforms.BLUE, Platforms.PURPLE, Platforms.YELLOW, Platforms.GREEN];