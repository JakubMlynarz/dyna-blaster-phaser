import * as Phaser from 'phaser';
import { GameScene  }  from "./gameScene";
import { SCREEN_HEIGHT } from "../const";
import { getRandomInt } from "../utils";
import { ALL_ENEMIES, Enemy } from "./enemies";

class EnemyEntity {
    sprite: Phaser.Physics.Arcade.Sprite;
    start: number;
    end: number;
    name: string;

    constructor(sprite: Phaser.Physics.Arcade.Sprite, start: number, end: number, name: string) {
        this.sprite = sprite;
        this.sprite.setSize(45, 45);
        this.sprite.setDisplaySize(45, 45);
        this.start = start;
        this.end = end;
        this.name = name;
    }
}
export class EnemyManager {
    enemies: EnemyEntity[] = [];
    gameScene: GameScene;
    enemiesGroup: Phaser.Physics.Arcade.Group;

    constructor(gameScene: GameScene) {
        this.gameScene = gameScene; 
        this.enemiesGroup = this.gameScene.physics.add.group();
        for (let i = 0; i < ALL_ENEMIES.length; i++) {
            this.addSpriteAnimation(ALL_ENEMIES[i])
        }
    }

    private addSpriteAnimation(enemy: Enemy) {
        this.gameScene.anims.create({
            key: enemy.name,
            frames: enemy.animFrames,
            frameRate: 10,
            repeat: -1,
          }); 
    }

    public addEnemy(platformIndex: number) {
        let enemy = this.getRandomEnemy();
        let platform = this.gameScene.plaformManager.platforms[platformIndex];
        let enemySprite = Phaser.Physics.Arcade.Sprite = this.enemiesGroup.create(platform.x ,platform.y - 50, enemy.frames[0]);
        let platformBounds = platform.getBounds();
        this.enemies.push(new EnemyEntity(enemySprite, platform.x - ((platformBounds.width / 2) - 10),platform.x + ((platformBounds.width / 2) - 10), enemy.name));
    }

    
    public addEnemOnGround(x: number) {
        let enemy = this.getRandomEnemy();
        let enemySprite = Phaser.Physics.Arcade.Sprite = this.enemiesGroup.create(x, SCREEN_HEIGHT - 120, enemy.frames[0]);
        this.enemies.push(new EnemyEntity(enemySprite, x - getRandomInt(150, 200), x + getRandomInt(200, 300), enemy.name));
    }

    public startEnemiesMove() {
        this.enemies.forEach((enemy) => {
            enemy.sprite.play(enemy.name, true);
            if (getRandomInt(0, 2) === 0) {
                enemy.sprite.setVelocityX(-100);
            } else {
                enemy.sprite.setVelocityX(100);
            }
        });
    }

    public changeEnemiesMove() {
        this.enemies.forEach((enemy) => {
            if(enemy.sprite.body !== undefined) {
                if(enemy.sprite.body.blocked.left || enemy.sprite.body.touching.left) {
                    enemy.sprite.setVelocityX(100);
                } else if(enemy.sprite.body.blocked.right || enemy.sprite.body.touching.right) {
                    enemy.sprite.setVelocityX(-100);
                } else {
                    if( enemy.sprite.x < enemy.start) {
                        enemy.sprite.setVelocityX(100);
                    } else if( enemy.sprite.x > enemy.end) {
                        enemy.sprite.setVelocityX(-100);
                    } 
                }
            }
        });
    }

    private getRandomEnemy(): Enemy {
        return ALL_ENEMIES[getRandomInt(0, ALL_ENEMIES.length)];
    }
}