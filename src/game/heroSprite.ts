import * as Phaser from 'phaser';
import { GameScene }  from "./gameScene";

export class HeroSprite {
    sprite: Phaser.Physics.Arcade.Sprite;
    gameScene: GameScene;
    xGridPos: number = 1;
    yGridPos: number = 1;
    tileSize: number;
    xGridSize: number;
    yGridSize: number;

    constructor(gameScene: GameScene, tileSize: number, xGridSize: number, yGridSize: number) {
      this.tileSize = tileSize;
      this.gameScene = gameScene;
      this.xGridSize = xGridSize;
      this.yGridSize = yGridSize;
      this.sprite = this.gameScene.physics.add.sprite(this.getXPos(), this.getYPos(), 'heroes','Bman_F_f00.png');
      this.sprite.setCollideWorldBounds(true);
      this.sprite.setGravityY(500);
      this.addSpriteAnimations();
    }

    getXPos(): number {
        return this.getPos(this.xGridPos);
    }

    getYPos(): number {
        return this.getPos(this.yGridPos);
    }

    private getPos(value: number) {
        return (value * this.tileSize) + this.tileSize / 2;
    } 

    jump() {
        this.sprite.setVelocityY(-600);
    }

    moveLeft() {
        this.sprite.setVelocityX(-200);
        this.sprite.play('left', true);
    }

    moveRight() {
       this.sprite.setVelocityX(200);
        this.sprite.play('right', true);
    }

    stopX() {
        this.sprite.setVelocityX(0);
    }

    private addSpriteAnimations() {
      this.gameScene.anims.create({
        key: 'down',
        frames: this.gameScene.anims.generateFrameNames('heroes', { prefix: 'Bman_F_f', zeroPad: 2, start: 0, end: 7, suffix: '.png' }),
        frameRate: 10,
        repeat: -1,
      });
      this.gameScene.anims.create({
        key: 'up',
        frames: this.gameScene.anims.generateFrameNames('heroes', { prefix: 'Bman_B_f', zeroPad: 2, start: 0, end: 3, suffix: '.png' }),
        frameRate: 10,
        repeat: -1,
      });
      this.gameScene.anims.create({
        key: 'right',
        frames: this.gameScene.anims.generateFrameNames('heroes', { prefix: 'Bman_R_f', zeroPad: 2, start: 0, end: 7, suffix: '.png' }),
        frameRate: 10,
        repeat: -1,
      });
      this.gameScene.anims.create({
        key: 'left',
        frames: this.gameScene.anims.generateFrameNames('heroes', { prefix: 'Bman_L_f', zeroPad: 2, start: 0, end: 7, suffix: '.png' }),
        frameRate: 10,
        repeat: -1,
      });
    }
  }