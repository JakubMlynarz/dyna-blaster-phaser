import * as Phaser from 'phaser';
import { HeroSprite } from "./heroSprite";
import { PlatformManager } from "./platformManager";
import { EnemyManager } from "./enemyManager";
import { SCREEN_WIDTH, SCREEN_HEIGHT, GAME_LENGTH} from "../const";
import { ALL_PLATFORMS, getPlatformAssetPath } from "./platforms";
import { ALL_ENEMIES, Enemy } from "./enemies";

const TILE_SIZE = 60;
const X_GRID_SIZE = SCREEN_WIDTH / TILE_SIZE;
const Y_GRID_SIZE = SCREEN_HEIGHT / TILE_SIZE;

const sceneConfig: Phaser.Types.Scenes.SettingsConfig = {
    active: false,
    visible: false,
    key: 'Game',
  };

class GameVariables {
  kills: number;
  lifes: number;
  startTime: Date;

  constructor() {
    this.kills = 0;
    this.lifes = 3;
    this.startTime = new Date();
  }
}

  function zfill(num, len) {return (Array(len).join("0") + num).slice(-len);}

export class GameScene extends Phaser.Scene {
    private ground: Phaser.GameObjects.TileSprite;
    private scoreText: Phaser.GameObjects.Text;
    private lifesText: Phaser.GameObjects.Text;
    private timerText: Phaser.GameObjects.Text;
    private gameVariables: GameVariables;
    private gate: Phaser.Physics.Arcade.Sprite;
    private music: Phaser.Sound.BaseSound;
    public boundsGroup: Phaser.Physics.Arcade.Group;
    public heroSprite: HeroSprite;
    public plaformManager: PlatformManager;
    public enemyManager: EnemyManager;
   
    constructor() {
      super(sceneConfig);
    }

    public preload() {
        this.load.image('sky', 'assets/sky1.png');
        this.load.image('ground', 'assets/ground.png');
        this.load.image('gate', 'assets/gate.png');
        this.loadPlatforms();
        this.loadEnemies();
        this.load.image('column', 'assets/column.png');
        this.load.atlas('heroes', 'assets/heroes.png', 'assets/heroes.json');
        this.load.audio('music', 'assets/music.mp3');
    }

    private loadPlatforms() {
      for (let pIndex in ALL_PLATFORMS) {
        this.load.image(ALL_PLATFORMS[pIndex], getPlatformAssetPath(ALL_PLATFORMS[pIndex]));
      }
    }

    private loadEnemies() {
      for (let eIndex in ALL_ENEMIES) {
        let enemy: Enemy = ALL_ENEMIES[eIndex];
        for (let fIndex in enemy.frames) {
          let frame: string = enemy.frames[fIndex];
          this.load.image(frame, 'assets/enemies/' + enemy.name + '/' + frame + '.png');
        }
      }
    }
   
    public create() {
      this.setupWorldAndBounds();
      this.gate = this.physics.add.staticSprite( GAME_LENGTH - 30, SCREEN_HEIGHT - 130, 'gate');
      this.music = this.sound.add('music')
      this.music.play();
      this.enemyManager = new EnemyManager(this);
      this.plaformManager = new PlatformManager(this);
      this.plaformManager.addPlatforms();
      this.heroSprite = new HeroSprite(this, TILE_SIZE, X_GRID_SIZE, Y_GRID_SIZE); 
      this.physics.add.collider(this.heroSprite.sprite, this.boundsGroup);
      this.physics.add.collider(this.enemyManager.enemiesGroup, this.boundsGroup);
      this.physics.add.collider(this.heroSprite.sprite, this.enemyManager.enemiesGroup, this.onCollisionWithEnemy, null, this);
      this.physics.add.collider(this.heroSprite.sprite, this.gate, this.gateReached, null, this);
      this.setupCamera();
      this.enemyManager.startEnemiesMove();
      this.gameVariables = new GameVariables();
      this.setupTextLabels();
    }

    private setupWorldAndBounds() {
      this.physics.world.setBounds(0,0, GAME_LENGTH, SCREEN_HEIGHT);
      this.add.tileSprite(SCREEN_WIDTH / 2 , SCREEN_HEIGHT / 2, GAME_LENGTH * 2, SCREEN_HEIGHT, 'sky');
      this.ground = this.add.tileSprite(SCREEN_WIDTH / 2 , SCREEN_HEIGHT - 40, GAME_LENGTH * 2, 80, 'ground');
      this.ground.setDepth(2);
      this.boundsGroup = this.physics.add.group({
        allowGravity: false,
        immovable: true,
      });
      this.boundsGroup.add(this.ground);
    }

    private setupCamera() {
      this.cameras.main.setBounds(0, 0, GAME_LENGTH, SCREEN_HEIGHT );
      this.cameras.main.startFollow(this.heroSprite.sprite);
    }

    private setupTextLabels() {
      this.scoreText = this.add.text(this.cameras.main.scrollX + 20 , 20, 'Score: ' + this.gameVariables.kills);
      this.lifesText = this.add.text(this.cameras.main.scrollX + 20, 50, 'Lifes: ' + this.gameVariables.lifes);
      this.timerText = this.add.text(this.cameras.main.scrollX + 20, 80, 'Time: 00:00');
    }

    public update() {
      this.performMove();
      this.enemyManager.changeEnemiesMove();
      this.updateTextLabels();
      if (!this.music.isPlaying) {
        this.music.play();
      }
    }

    private performMove() {
      const cursorKeys = this.input.keyboard.createCursorKeys();
      if (cursorKeys.right.isDown ) {
        this.heroSprite.moveRight();
      } else if (cursorKeys.left.isDown ) {
        this.heroSprite.moveLeft();
      } else {
          this.heroSprite.stopX();
      }
    
      if ((cursorKeys.space.isDown || cursorKeys.up.isDown) && this.heroSprite.sprite.body.touching.down ) {
        this.heroSprite.jump();
      }
    }

    private updateTextLabels() {
      this.scoreText.setX(this.cameras.main.scrollX + 20);
      this.lifesText.setX(this.cameras.main.scrollX + 20);
      this.timerText.setX(this.cameras.main.scrollX + 20);
      this.updateTimer();
    }

    private updateTimer() {
      this.timerText.setText('Time: ' + this.getTime());
    }

    private getTime(): string {
      const diffInSec = Math.abs(new Date().getTime() - this.gameVariables.startTime.getTime()) / 1000;
      var minutes = Math.floor(diffInSec / 60);
      var seconds = Math.floor(diffInSec) - (60 * minutes);
      return zfill(minutes, 2) + ":" + zfill(seconds, 2);
    }

    private onCollisionWithEnemy(player: Phaser.Physics.Arcade.Sprite, enemy: Phaser.Physics.Arcade.Sprite) {
      if(player.body.touching.down === true && player.body.touching.right === false && player.body.touching.left === false) {
        this.gameVariables.kills++;
        this.scoreText.setText('Score: ' + this.gameVariables.kills);
        enemy.destroy();
      } else {
        this.gameVariables.lifes--;
        this.lifesText.setText('Lifes: ' + this.gameVariables.lifes);
        this.heroSprite.sprite.setPosition(10, 10);
        if (this.gameVariables.lifes <= 0) {
          this.gameVariables.lifes = 0;
          this.music.stop();
          this.scene.start('GameOver', {score: this.gameVariables.kills});
        }
      }
    }

    private gateReached() {
      this.scene.start('GameEnd', {score: this.gameVariables.kills, time: this.getTime()});
    } 
  }