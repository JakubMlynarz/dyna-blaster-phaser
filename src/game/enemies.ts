import * as Phaser from 'phaser';

export class Enemy {
    name: string;
    frames: string[] = [];
    animFrames: Phaser.Types.Animations.AnimationFrame[] = [];
    constructor(name: string, framesNumber: number) {
        this.name = name;
        for( let i = 0; i < framesNumber; i++ ) {
            this.frames.push(name + i.toString())
            this.animFrames.push({
                key: name + i.toString(),
                frame: name + i.toString()
            });
        }
    }
}

export const ALL_ENEMIES: Enemy[] = [new Enemy('tom', 6), new Enemy('john', 8), new Enemy('kevin', 9)];
